from data_handler import DataHandler
from chaos import ChaosTest
from date_processor import processor
import logging
import settings

logging.basicConfig(filename=settings.LOG_FILE,
                    format="%(asctime)s [%(levelname)s] %(message)s")

def run():
    with DataHandler() as dh, ChaosTest():
        dh.ads = processor.process_all_ads(dh.ads)


if __name__ == '__main__':
    run()
