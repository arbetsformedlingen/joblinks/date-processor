import sys
from datetime import datetime
import logging
from dateutil import parser
import settings


def now():
    return datetime.now().strftime("%Y-%m-%dT%H:%M:%S")


def _isodate(bad_date):
    if not bad_date:
        return None
    try:
        date = parser.parse(bad_date)
        return date.isoformat()
    except ValueError as e:
        print_and_log_error(f'Failed to parse: {bad_date} as a valid date. Using current timestamp. Error: {e}')
        return now()


def check_ad_for_future_date(ad, change_future=False):
    date_from_ad = ad['date_to_display_as_publish_date']
    first_seen = ad['firstSeen']
    if date_from_ad > now():
        msg_prefix = f"Date: {date_from_ad} in ad: {ad['id']} is in the future. "
        sources = f"  Sources: {get_sourcelinks_from_ad(ad)}"
        if change_future:
            print_and_log_warning(f"{msg_prefix} Changing date to 'firstSeen': {first_seen}  {sources}")
            ad['date_to_display_as_publish_date'] = first_seen
        else:
            logging.info(f"{msg_prefix} Date is not changed. {sources}")
    return ad


def get_sourcelinks_from_ad(ad):
    source_links = []
    for link in ad['sourceLinks']:
        source_links.append(link['displayName'])
    return source_links


def set_display_date_in_ad(ad):
    date_from_ad = ad['originalJobPosting'].get('datePosted', None)
    if not date_from_ad:
        date_from_ad = ad.get('firstSeen', None)
        print_and_log_warning(f"No 'datePosted' date in ad: {ad['id']}, using 'firstSeen': {date_from_ad}")
    if not date_from_ad:
        date_from_ad = now()
        print_and_log_warning(f"No 'firstSeen' date in ad: {ad['id']}, using current timestamp: {date_from_ad}")

    # add new field
    ad['date_to_display_as_publish_date'] = _isodate(date_from_ad)
    return ad


def process_all_ads(all_ads):
    logging.info("start processing ads")
    converted_ads = []
    for ad in all_ads:
        ad_with_date = set_display_date_in_ad(ad)
        checked_ad = check_ad_for_future_date(ad_with_date, settings.CHANGE_FUTURE_DATES)
        converted_ads.append(checked_ad)
    logging.info("all ads processed")
    return converted_ads

def print_and_log_warning(warning_msg):
    logging.warning(warning_msg)
    print(f"WARNING - {warning_msg}", file=sys.stderr)

def print_and_log_error(error_msg):
    logging.error(error_msg)
    print(error_msg, file=sys.stderr)
