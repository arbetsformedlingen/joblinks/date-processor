import pytest
from datetime import timedelta, datetime

from date_processor import processor as dp


@pytest.mark.parametrize("date_posted", [None, '2021-02-04T10:09:37'])
def test_display_date_only_original(date_posted):
    """
    Check that 'date_to_display_as_publish_date' has correct value
    if 'originalJobPosting'.'datePosted' does not exist (None),
    'date_to_display_as_publish_date' should get a value (which is not checked
    in detail because exact value is set at runtime)
    'firstSeen' should not be changed
    """
    ad = {'id': '123', 'firstSeen': date_posted, 'originalJobPosting': {'datePosted': date_posted}}
    result = dp.set_display_date_in_ad(ad)
    assert result['originalJobPosting']['datePosted'] is date_posted
    assert result['firstSeen'] is date_posted
    if date_posted:
        assert ad['date_to_display_as_publish_date'] == '2021-02-04T10:09:37'
    else:
        assert ad['date_to_display_as_publish_date'] is not None


def test_display_date_first_seen():
    """
    Check that 'date_to_display_as_publish_date' is set and that 'firstSeen' is not changed
    """
    date_value = '2021-02-04T10:09:37'
    ad = {'id': '123', 'firstSeen': date_value, 'originalJobPosting': {'datePosted': None}}
    result = dp.set_display_date_in_ad(ad)
    assert result['originalJobPosting']['datePosted'] is None
    assert result['date_to_display_as_publish_date'] == date_value
    assert result['firstSeen'] == date_value


def test_display_date_both():
    """
    Check that date is set correctly and that 'firstSeen' is not changed
    """
    date_value = '2021-02-04T10:09:37'
    ad = {'id': '123', 'firstSeen': date_value, 'originalJobPosting': {'datePosted': date_value}}
    result = dp.set_display_date_in_ad(ad)
    assert result['originalJobPosting']['datePosted'] == date_value
    assert result['date_to_display_as_publish_date'] == date_value
    assert result['firstSeen'] == date_value


@pytest.mark.parametrize("change", [True, False])
def test_future_date(change):
    """
    Check that a date that is in the future is changed if that parameter is used
    (normally controlled by environment variable)
    """
    future_date = (datetime.now() + timedelta(days=2)).strftime("%Y-%m-%dT%H:%M:%S")
    ad = {'id': '123', 'originalJobPosting': {'datePosted': None}, 'date_to_display_as_publish_date': future_date,
          'sourceLinks': [], 'firstSeen': '2021-02-04T10:09:37'}
    result = dp.check_ad_for_future_date(ad, change_future=change)
    assert change == (result['date_to_display_as_publish_date'] != future_date)
