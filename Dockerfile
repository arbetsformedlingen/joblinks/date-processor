FROM docker.io/library/python:3.12.6-slim-bookworm AS base


WORKDIR /app

COPY . /app

RUN apt-get -y update &&\
    python -m pip install --upgrade pip poetry && python -m poetry config virtualenvs.create false &&\
    python -m poetry install


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN python -m pytest -vv &&\
    python main.py < /testdata/input.10 > /tmp/output.10 &&\
    diff /testdata/expected_output.10 /tmp/output.10 \
    && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

ENTRYPOINT ["python","main.py"]
