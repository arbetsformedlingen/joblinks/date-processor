# date-processor

Handles dates in ads, with option to correct them if they are in the future

In this pipeline step, a new ad field `date_to_display_as_publish_date` is added and set to one of these values:

Priority:

1. `originalJobPosting.datePosted`
2. `firstSeen`
3. `now`

If the date is in the future, it can be set to `firstSeen` (controlled by environment variable `CHANGE_FUTURE_DATES`).

When running as part of the JobLinks pipeline, the ads are received through stdin and sent to the next pipeline step through stdout.

## Prerequisites

Read the [Python guidelines](https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/python-projects.md)

Install dependencies with `poetry install`

## Environment variables

| variable              | comment
|-----------------------|---------
| `USE_STDIN`           | default value `False`
| `FILE_NAME`           | default value `output.json`, used if `USE_STDIN` is `False`
| `LOG_LEVEL`           | default value `INFO`
| `LOG_FILE`            | default value `date-processor-log`
| `PRINT`               | default value `True`. Determines if ads should be written to stdout at completion.
| `CHANGE_FUTURE_DATES` | default value `False`. Determines if a date from an ad should be to `firstSeen` if it is in the future.

## Logging

Logging is done to file. Dates in the future will be logged at level _warning_.

## Tests

```shell
python -m pytest tests
```

## Usage

```shell
python main.py
```

Optional argument `--chaos-test` will cause 5% risk of exiting immediately with error code `1`.
